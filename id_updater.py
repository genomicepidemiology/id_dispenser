import pymysql.cursors
import argparse
import os
import sys
import pandas as pd


class ID_DATABASE:

    def __init__(self, password, mode):

        self.start_count = 1000000
        self.end_count = 9999999
        self.table = "sample_id_list"
        self.database = "genepi"
        self.connect_database(password)
        if mode == "Initiate":
            self.create_table_users()
            print("Database created")
        elif mode == "Reset":
            self.delete_tables()
            self.create_table_users()
            print("Database reseted")
        elif mode == "Delete":
            self.delete_tables()
        else:
            self.check_db()

    def connect_database(self, password):
        """ Connect to MySQL database """
        self.conn = pymysql.connect(host="XXXXX",
                                    db='XXXX',
                                    user='XXXX',
                                    port="XXXX",
                                    charset='utf8mb4',
                                    password=password)

        if self.conn.open:
            print('Connected to MySQL database')
        else:
            self.close_mysql()

    def create_table_users(self):
        """Create id table"""
        cursor = self.conn.cursor()
        cursor.execute("""CREATE TABLE IF NOT EXISTS {0}
                          (sample_id INT PRIMARY KEY,
                          user_name VARCHAR(10) NOT NULL,
                          time timestamp)
                        """.format(self.table))
        updater = """INSERT INTO {1} (sample_id, user_name, time)
                        VALUES({0}, 'init_entry', CURRENT_TIME());
                        """.format(self.start_count, self.table)
        cursor.execute(updater)

    def delete_tables(self):
        "Delete tables of the database"
        cursor = self.conn.cursor()
        cursor.execute("DROP TABLE {0};".format(self.table))

    def extract_ids(self, user, number, output_folder=None):
        """Extract id's from database and write to tab file"""
        cursor = self.conn.cursor()
        last_id = self.get_last_id()
        extracted_ids = []
        for i in range(number):
            float_id = last_id+i+1
            self.check_length(id=float_id+i+1)
            updater = """INSERT INTO {2} (sample_id, user_name, time)
                        VALUES({1},'{0}',CURRENT_TIME());
                        """.format(user, float_id, self.table)
            cursor.execute(updater)
            print(">", float_id, user)
            extracted_ids.append([user, float_id])
        if output_folder is not None:
            df1 = pd.DataFrame(extracted_ids)
            df1.to_excel(output_folder+"/idsamples.xlsx", index=None,
                         header=None)
        print("Extracted {0} samples for the user {1}".format(number, user))

    def close_mysql(self, save=False):
        """Close mysql database"""
        if self.conn is not None and self.conn.open:
            if save:
                print("Database saved")
                self.conn.commit()
            self.conn.close()
            print("Database closed")

    def check_db(self):
        """Check tables are coordinated"""
        cursor = self.conn.cursor()
        cursor.execute("""SELECT sample_id FROM {0} GROUP BY sample_id
                        HAVING COUNT(*) > 1;""".format(self.table))
        records = cursor.fetchall()
        if len(records) == 0:
            print("The table of id's do not have duplicated entries")
            pass
        else:
            self.close_mysql()
            sys.exit("The table of id's do have duplicated entries")
        last_id = self.get_last_id()
        self.check_length(id=last_id, print_out=True)

    def get_last_id(self):
        """Get last row of database"""
        cursor = self.conn.cursor()
        exec = """SELECT sample_id FROM {0} ORDER BY sample_id DESC LIMIT 1;
               """.format(self.table)
        cursor.execute(exec)
        last_id = cursor.fetchone()[0]
        return last_id

    def check_length(self, id, print_out=False):
        """Check length of the database"""
        if id >= self.end_count:
            self.close_mysql(save=True)
            sys.exit("Database COMPLET")
        elif id < self.end_count and print_out:
            ids_left = int(self.end_count)-int(id)
            print("{} sample IDs left".format(ids_left))
        else:
            pass

    def show_entries(self, output_folder=None, users=None, ids=None,
                     all=False):
        """Show entries"""
        cursor = self.conn.cursor()
        if all:
            command = """SELECT * FROM {0} ORDER BY sample_id
                      """.format(self.table)
            cursor.execute(command)
            records = cursor.fetchall()
        else:
            if users is None and ids is not None:
                command = """SELECT * FROM {1} WHERE sample_id IN ({0})
                            ORDER BY sample_id""".format(ids, self.table)
                cursor.execute(command)
                records = cursor.fetchall()
            elif users is not None and ids is None:
                users = '"'+'","'.join(users.split(","))+'"'
                command = """SELECT * FROM {1} WHERE user_name IN ({0})
                            ORDER BY sample_id""".format(users, self.table)
                cursor.execute(command)
                records = cursor.fetchall()
            else:
                command = """SELECT * FROM {2} WHERE (user_name IN {0} AND
                             sample_id IN {1}) ORDER BY sample_id
                            """.format(users, ids, self.table)
                cursor.execute(command)
                records = cursor.fetchall()

        if output_folder is not None:
            df1 = pd.DataFrame(list(records))
            df1.to_excel(output_folder+"/idsamples.xlsx", index=None,
                         header=None)
        for i in records:
            a = " ".join(str(x) for x in i[:-1])
            print("> "+a)


def get_arguments():
    """Parse the arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument('-out', '--output_folder',  help="""folder to store the
                        output""", default=None)
    parser.add_argument('-mode', '--database_mode', help="""What to do with
                        the database""", default='Extract',
                        choices=["Initiate", "Reset", "Extract", "Delete",
                                 "Show"])
    parser.add_argument('-psswd', '--password', help="""Password for the
                        database as user""", type=str, required=True)
    parser.add_argument('-user', '--user_id', help="""User that requires ids
                        from database""", default=None)
    parser.add_argument('-ids', '--number_ids', help="""Number of ids extracted
                        from database""", default=None)
    parser.add_argument('-excel', '--excel_file', help="""Excel file with two
                         columns: user and id""", action='store_true',
                        default=False)
    parser.add_argument('-all', '--show_all', help="""Show all database""",
                        action='store_true', default=False)
    parser.add_argument('-unsafe', '--unsafe_mode', help="Allow introducing "
                        "more than 1000 ids", action='store_true',
                        default=False)
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = get_arguments()
    if (args.output_folder is None and args.excel_file):
        sys.exit("No output folder was selected")
    if args.output_folder is not None:
        out_folder = os.path.abspath(args.output_folder)
    else:
        out_folder = None
    if args.show_all and args.database_mode != "Show":
        sys.exit("Option 'show all' has to be selected with 'mode Show'")
    if (args.show_all) and (args.number_ids is not None
                            or args.user_id is not None):
        sys.exit("Option 'show all' cannot be selected with user and ids")
    id_db = ID_DATABASE(password=args.password, mode=args.database_mode)
    if args.database_mode == "Extract":
        if args.user_id is None:
            id_db.close_mysql()
            sys.exit("User id required to extract id's")
        elif args.number_ids is None:
            id_db.close_mysql()
            sys.exit("Number of id's required to extract id's")
        else:
            try:
                number = int(args.number_ids)
            except ValueError:
                id_db.close_mysql()
                sys.exit("The amount of IDs has to be a number without "
                         "decimals")
            if args.unsafe_mode:
                id_db.close_mysql()
                sys.exit("In safe mode, no more than 1000 id's can be created")
            id_db.extract_ids(user=args.user_id, number=number,
                              output_folder=out_folder)
            id_db.check_db()
            id_db.close_mysql(save=True)
    elif args.database_mode == "Show":
        if (args.user_id is None and args.number_ids is None
                and args.show_all is None):
            id_db.close_mysql()
            sys.exit("No user or id's or 'show all option' have been selected "
                     "to show")
        elif (args.user_id is not None and args.number_ids is not None):
            id_db.close_mysql()
            sys.exit("Both user and id's have been selected to show. Choose "
                     "only one.")
        else:
            id_db.show_entries(output_folder=out_folder, ids=args.number_ids,
                               users=args.user_id, all=args.show_all)
        id_db.close_mysql()
    else:
        id_db.close_mysql(save=True)
