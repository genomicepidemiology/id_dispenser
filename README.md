ID-dispenser documentation
=============

This repository contains the python code to control a MySQL database that dispense ID numbers

## Content of the repository

1. id_updater.py - the program

## Installation

Setting up ID-dispenser script and database
```bash
# Go to wanted location for id-dispenser
cd /path/to/some/dir

# Clone and enter the id-dispenser directory
git clone https://git@bitbucket.org/genomicepidemiology/id_dispenser.git
cd id_dispenser
```
## Usage

You can run id-dispenser command line using python3
```bash
usage: id_updater.py [-h] [-out OUTPUT_FOLDER]
                           [-mode {Initiate,Reset,Extract,Delete,Show}] -psswd
                           PASSWORD [-user USER_ID] [-ids NUMBER_IDS] [-excel]
                           [-all] [-unsafe]

optional arguments:
  -h, --help            show this help message and exit
  -out OUTPUT_FOLDER, --output_folder OUTPUT_FOLDER
                        folder to store the output
  -mode {Initiate,Reset,Extract,Delete,Show}, --database_mode {Initiate,Reset,Extract,Delete,Show}
                        What to do with the database
  -psswd PASSWORD, --password PASSWORD
                        Password for the database as user
  -user USER_ID, --user_id USER_ID
                        User that requires ids from database
  -ids NUMBER_IDS, --number_ids NUMBER_IDS
                        Number of ids extracted from database
  -excel, --excel_file  Excel file with two columns: user and id
  -all, --show_all      Show all database
  -unsafe, --unsafe_mode
                        Allow introducing more than 1000 ids
```

### Web-server

A webserver implementing the methods is available at the [CGE
website](http://www.genomicepidemiology.org/) and can be found here:
https://cge.cbs.dtu.dk/services/ResFinder/

### Documentation

The documentation available as of the date of this release can be found at
https://bitbucket.org/genomicepidemiology/resfinder/overview.

License
=======

Copyright (c) 2014, Ole Lund, Technical University of Denmark
All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.